#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*
 * This file is released under the 3-clause Revised BSD license.
 * Copyright (c) 2015, Francesco Invernici.
 * All rights reserved
 **********************
 * Lezione di lun 18 gen 2016, 14.57.58, CET
*/

//struct elem {
//    char *parola;
//    int freq;
//};

//(struct freq_parola) *freqs(char *frase){}

char* copian(char* from, char* to){
    char* res=(char*)malloc(sizeof(char)*(to-from)+1);
    int i;
    for(i=0; i<to-from; i++){
        res[i]=from[i];
    }
    res[i]='\0';
    return res;
}

int isLetter(char c){
    return (c<='z' && c>='a') ||
           (c<='Z' && c>='A') ||
           (c>='0' && c<='9');
}
char *ppp(char *i){
        while(isLetter(*i)) {i++;}
        if(*i == '\0') return NULL;
        while(!isLetter(*i)) {i++;}
        return i;
}
//Accede all'ennesima parola all'interno di una frase e ne ritorna una copia
//assert(strcmp(parola("ciao a tutti"), "tutti") == 1);
char *parola(char *frase, int n){ 
    if(n==0){
        char *i = frase;
        while(isLetter(*i)) {i++;}
        return copian(frase, i);
    }
    else{
        char *i=ppp(frase);
        if(i==NULL) return NULL;
        return parola(i, n-1);
    }
}
int conta_parole(char *word, char *str){
int i=0, j=0;
for(j=0; parola(str,j) != NULL; j++){
        if(strcmp(word, parola(str,j)) ==0)
            i++;
    }
    return i;
}

char *max_parola(char *frase){
    char *max_str = NULL;
    int max = 0, i;
    for(i=0;parola(frase,i)!=NULL;i++){
        int n = conta_parole(parola(frase, i), frase);
        if(n > max){
             max=n;
             max_str=parola(frase,i);
        }
    }
    return max_str;
}

void main(){

printf("%s\n",max_parola("ciao ciao a tutti"));
}
