#include <assert.h>
#include <string.h>
#include <stdio.h>
#include "34-add-base.c"

///***TEST DRIVEN DESIGN
//REGRESSION TEST
//Compilare con 34-add-base
//
//char *add(char *n1, char *n2);

void require(char *msg, int c){
    printf("%s -> %s\n", msg, c);
    assert(c);
}

int main(){
    require("2+3=5", strcmp(add("2","3"), "5") == 0);
    require("2+0=2", strcmp(add("2","0"), "2") == 0);
    require("11+13=24", strcmp(add("11","13"), "24") == 0);
    require("1+1=2", strcmp(add("1","1"), "2") == 0);
    require("10+1=11", strcmp(add("10","1"), "11") == 0);
    require("100+1=101", strcmp(add("100","1"), "101") == 0);
    require("1010+1=1011", strcmp(add("1010","1"), "1011") == 0);
    return 0;
}
