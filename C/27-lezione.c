#include <stdio.h>
/*
 * This file is released under the 3-clause Revised BSD license.
 * Copyright (c) 2015, Francesco Invernici.
 * All rights reserved
 **********************
 * Lezione di lun 21 dic 2015, 11.41.22, CET
 */

//SUDOKU SOLVER
//"Albero delle scelte" come modello
//Universo: Mettendo un numero posso accedere a 9 sottouniversi e così via
//Ipotizziamo che ci siano 56 posti liberi --> sono 9^56 possibili universi finali. Troppo tempo.
//Si devono cercare delle strategie di pruning per scartare preventivamente gli universi dove si perde. Alla fine si ottiene l'unico universo possibile.
//Teoria del multiverso di Everett
//Se la prova è sbagliata, aggiungi 1 all'ultima casella
//Oppure breath --> Faccio tutte le scelte al primo livello e controllo, scarto quelle perse e continuo al passo 2 da quelle vincenti
//Si deve potare l'albero --> Più efficiente

//Gli passo la matrice incompleta, ritorna la matrice risolta
//Array di array
//Sudoku di ordine n ha un lato di n*n

#define O 2
#define DIM (O*O)

int corretto(int m[DIM][DIM]){
}
int *prima_libera(int m[DIM][DIM]){
//RITORNA PUNTATORE A INT o NULL, se non c'è niente di libero
}
int risolvi(int m[DIM][DIM]){
    int *p;
//Controllo se c'è qualche regola violata, se arriva un sudoku non risolvibile esce subito
//corretto?: 1 se risolvibile, 0 non risolvibile
    if (corretto(m) == 0)
        return 0;
    
    p = prima_libera(m);
    if(p == NULL)
        return 1;
/*    *p = 1;
    if(risolvi(m)) return 1;

    *p = 2;
    if(risolvi(m)) return 1;
//Lo integro in un ciclo for
*/
    for(i=1;i<=DIM;i++){
        *p=1;
        if(risolvi(m) return 1;
                }
     *p = 0;
     return 0;


//Prendi la prima casella vuota e mettici 1
//    m[qui][qua] = 1;
    //Corretto? e così via
}
//Di quanta memoria ha bisogno?    
void main(){
m = [[0,2,0,4],[0,3,0,0],[1,0,0,0],[0,4,0,3]]
}
