#include <stdio.h>
/*
 * This file is released under the 3-clause Revised BSD license.
 * Copyright (c) 2015, Francesco Invernici.
 * All rights reserved
 **********************
 * Lezione di lun 14 dic 2015, 11.34.18, CET
 *
 * ALLOCAZIONE MEMORIA PER STRINGHE E PROBLEMI CON MALLOC
 * Es. Catena telefonica, numeri di telefono come indirizzi
 * Nel primo posto (primi due) lascio 0, chiedo a malloc 2 parole, salvo l'indirizzo della prima data nel mio posto vuoto all'inizio, dove non contiene il suo indirizzo.
 * L'ultimo non ha il numero di telefono di nessuno. Per convenzione nella seconda parola ha tutti i bit a 0 --> NULL addr.
 * Così si può utilizzare la memoria dinamicamente e non allocandola in massa prima del bisogno.
 * Questa struttura dati si chiama LISTA LINKATA ==> valore + puntatore
 * I blocchi della lista sono NODI
 * Puntatore al primo nodo --> leggo --> puntatore al secondo nodo
 * Millesimo elemento della lista: *(a+999)
 * NODO si rappresenta come blocco diviso in due parti: valore e puntatore al prossimo (freccia verso l'esterno o verso il prossimo)
 * Definire nuovo tipo di dato con due campi: struct persona{ char* nome; int numero;};
 * Posso dichiarare variabili con questo tipo e usarle normalmente
 * Come accedo ai campi? nome_variabile.nome_campo = "dato da inizializzare"
 * Ogni oggetto struct persona in memoria avrà la prima parte contenente il primo campo, un puntatore a caratteri
 * Quanto occupa ogni elemento in memoria? sizeof(char* ) + sizeof(int)
 * Come punto alla variabile? Puntatore a struct persona: struct persona a; struct persona *p;
 * (*p).numero = 2;
 * (*p).nome ) = "nome";
 * Dato i puntatori a una struttura sono frequenti, in C, per convenzione si usa l'operatore ->
 * p->numero
 * p->nome
 * Il miglior modo per rappresentare un nodo di una lista è una struct
 * Es. nodo di lista di interi
 */ struct nodo{
   int valore;
   struct nodo *prossimo;
  }
 
  int main(){
  struct nodo *testa = NULL //NULL è inizializzato in stdio.h
  struct nodo elemento_1;
  elemento_1.valore = 12;
  elemento_1.prossimo = NULL;
  testa = &elemento_1
  elemento_1.prossimo = malloc(sizeof(struct nodo));
  (*(elemento_1.prossimo)).valore = 23;
  (*(elemento_1.prossimo)).prossimo = NULL;
  elemento_1.prossimo->prossimo = malloc(sizeof(struct nodo));
  //(*(elemento_1.prossimo)).prossimo = malloc(sizeof(struct nodo));

  }
