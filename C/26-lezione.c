#include <stdio.h>
#include <stdlib.h>
/*
 * This file is released under the 3-clause Revised BSD license.
 * Copyright (c) 2015, Francesco Invernici.
 * All rights reserved
 **********************
 * Lezione di mar 15 dic 2015, 15.45.13, CET
 */
//LNODE
//Quando ho una testa, posso trovare l'intera lista passando dal campo prossimo
//Testa prossimo = a quello nuovo, se ho una lista di un solo elemento
//---> Per induzione posso mettere un nuovo elemento in fondo a qualsiasi lista

struct nodo {
    int valore;
    struct nodo *prossimo
};

void print_list(struct nodo *head){
    if(head->prossimo == NULL){
        printf("%i\n", head->valore);
        return;}
    printf("%i -> ", head->valore);
    print_list(head->prossimo);
}

void append(int n, struct nodo *head){
    if (head->prossimo == NULL){
        head->prossimo = malloc(sizeof(struct nodo));
        head->prossimo->valore = n;
        head->prossimo->prossimo = NULL;
        return;
    }//La lista è lunga 1
    append(n, head->prossimo);
}
struct nodo *last(struct nodo *head){
    if(head->prossimo == NULL) {
        return head;}
    return last(head->prossimo);
}

void push(int n, struct nodo **testa){
    struct nodo *new_elem = malloc(sizeof(struct nodo));
    new_elem->valore = n;
    new_elem->prossimo = *testa;
    **testa = new_elem;
}

int main() {
    struct nodo elemento;
    struct nodo *testa;
    struct nodo *tmp;

    elemento.valore = 13;
    elemento.prossimo = NULL;

    print_list(&elemento);
    append(42, &elemento);
    int i;
    for(i=0;i<20;i++) {
        append(i*i, &elemento);}
    //last(&elemento)->prossimo = &elemento; //LOOP
    print_list(&elemento);
    
    print_list(testa);
    tmp = malloc(sizeof(struct nodo));
    tmp->valore = 100;
    tmp->prossimo = testa;
    testa = tmp;
    print_list(testa);
    for(i=0;i<10;i++){
        push(i*i*i, testa);
    }
    return 1;
}
