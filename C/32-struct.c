#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "32-lezione.c"


typedef struct _elem {
    char *key;
    int data;
    struct _elem *next;
} elem;

elem *get(elem *d, char *key){
    if(d==NULL) return NULL;
    if(strcmp(d->key, key) == 0) return d;
    return get(d->next, key);
}
