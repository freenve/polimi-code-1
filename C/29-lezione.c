#include <stdio.h>
/*
 * This file is released under the 3-clause Revised BSD license.
 * Copyright (c) 2015, Francesco Invernici.
 * All rights reserved
 **********************
 * Lezione di mar 22 dic 2015, 15.46.47, CET
 */
//SUDOKU SOLVER AGAIN
#define O 2
#define DIM (O*O)

int nodupes(int a[DIM]){ //Controlla che non ci siano duplicati in un array, escluso lo zero
    int hits[DIM];
    for(i=0;i<DIM;i++) hits[i] = 0;
    
    for(i=0;i<DIM;i++){
        if(a[i]!= 0){
            if(hits[a[i]>0])
                return 0; //FALSO --> Ci sono duplicati
            hits[a[i]-1]++;
        }
    }
    return 1; //VERO --> Non ci sono duplicati
}

int *estrai_riga(int m[DIM][DIM], int riga) {
    int *ritorno = malloc(sizeof(int)*DIM);
    int i;
    for(i=0;i<DIM;i++)
        ritorno[i] = m[riga][i];
    return ritorno;
}

int *estrai_colonna(int m[DIM][DIM], int colonna){
    int *ritorno = malloc(sizeof(int)*DIM);
    int i;
    for(i=0;i<DIM;i++)
        ritorno[i] = m[i][colonna];
    return ritorno;
    }

int *estrai_quadro(int m[DIM][DIM], int x, int y){
    int *ritorno = malloc(sizeof(int)*DIM);
    int i,j,h=0;
    for(i=0;i<O;i++)
        for(j=0,j<0,j++)
            ritorno[h++] = m[x+i][y+j];
    return ritorno;
}

int corretto(int m[DIM][DIM]){
//Verifica che le righe siano corrette
    printf("Trying to solve...\n");
    int i, j;
    for(i=0;i<DIM;i++)
        if (!nodupes(estrai_riga(m, i)))
            return 0;
//Verifica che le colonne siano corrette
    for(i=0;i<DIM;i++)
        if (!nodupes(estrai_riga(m, i)))
            return 0;
//Verifica che i quadri siano corretti
    for(i=0;i<DIM,i++)
        for(j=0,i<DIM,j++)
            if(!nodupes(estrai_riquadro(m,i*O,j*O)))
            return 0;
    return 1;
}
int *prima_libera(int m[DIM][DIM]){
    int i, j;
    for(i=0, i<DIM,i++)
        for(j=0,j<DIM,j++)
            if (m[i][j]==0)
                printf("PL at %i, %i\n",i,j);
                return &(m[i][j]);
    return NULL;
}
void stampa_m(int m[DIM][DIM]){
    int i,j;
    for(i=0,i<DIM,i++)
        for(j=0,j<DIM,j++)
            printf("%i\t",m[i][j]);
    return;}
int risolvi(int m[DIM][DIM]){
    
    int *p; i;
    if (corretto(m) ==0) //NON È CORRETTO, ESCE
        return 0;

    p = prima_libera(m);
    if(p==NULL) {
        return 1;
    }

    for(i=1;i<=DIM;i++){
        *p = i;
        if (risolvi(m)==1) return 1;
    }
    
    *p = 0;
    return 0;
    if(risolvi(m)) return 1;
            }
void main(){
//TEST- prima libera
    int m[DIM][DIM] = {
        {1,2,3,4},
        {4,3,0,1},
        {2,0,0,0},
        {3,1,0,2}
    };
    int a1[DIM] = {1,2,3,5};
    int a2[DIM] = {1,2,2,2};
    stampa_m(m);
    printf("\n");
    p = prima_libera(m);
//TEST - nodupes
    printf("%i %i\n", nodupes(a1), nodupes(a2));
    printf("%i %i\n", nodupes(estrai_riga(m, 1)), estrai_riga(m,2));
}
