#include <stdio.h>
/*
*This file is released under the 3-clause Revised BSD license.
*Copyright (c) 2015, Francesco Invernici.
*All rights reserved
***
*Lezione di lun 30 nov 2015, 14.34.55, CET
*/
//& = L'indirizzo di
//* = Il puntatore a
#define N 5

void stampa(int x, int y){ //STAMPA
    printf("a = %d\t b = %d\n", x, y);
}

int swap(int *val_1, int *val_2){ //SWAP
    stampa(*val_1, *val_2); //ORA SWAPPA VARIABILI DI AMBIENTE ESTERNO
    int tmp;
    tmp = *val_1;
    *val_1 = *val_2;
    *val_2 = tmp;
    stampa(*val_1, *val_2);
}

/*void main(){
    int a, b;
//    a = 5;
//    b = 7; 
    printf("Inserisci il primo numero: ");
    scanf("%d", &a); //La SCANF prende indirizzi di variabili, non variabili
    printf("Inserisci il secondo numero: ");
    scanf("%d", &b);

//    a_pointer = &a;
//    b_pointer = &b;
    swap(a,b);
//   swap(a_pointer, b_pointer);
}*/

int main(){
    int i,n;
//    int array[N];
    int *p;

    printf("Inserisci il numero di valori: ");
    scanf("%d", &n);

    p = malloc(sizeof(int)*n);
/*
    for(i=0; i<N; i++)
        array[i]=i;
    for(i=0; i<N; i++)
        printf("%d\n", array[i]);
*/

    for(i=0; i<n; i++)
        *(p+i)=i;

    for(i=0; i<n; i++)
        printf("%d at %p \n", *(p+i), &p); //%p è l'indirizzo in esadecimale
        
}

//Calcolo spreco di risorse: N-usati/N-allocati
