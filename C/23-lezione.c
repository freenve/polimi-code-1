#include <stdio.h>
/*
* This file is released under the 3-clause Revised BSD license.
* Copyright (c) 2015, Francesco Invernici.
* All rights reserved
*
* Lezione di ven 11 dic 2015, 10.52.08, CET
*/

//Da rivedere:Puntatori e puntatori a puntatori

//stampa_stringhe([array di stringhe]) --> stampa e basta
void stampa_stringhe(char* a[], int len) {
//    printf("%s", a[0]);
    for(int i=0; i<len;i++)
        printf("%s\n",a[i])
}

//ordina_interi(int a[], len)
/* Come fare il sorting
 * Cerco il minimo e lo metto in cima(swap)
 * Inizio dal secondo elemento e itero
 ***********
 * Oppure
 * Sono in ordine?
 * Bubble sort o sick sort
 * 1-2 sono in ordine? Se sì, swap, se no, no. Lo fa fino alla fine. Ripete, finché non è ordinato.
 */
void ordina_interi(int a[], int len) {
    int i;
    for(int j=0;j<len;j++){
        for(i=0; i<len-1,i++){
            if(a[i+1] < a[i]){
                int tmp; //swappa gli elementi
                tmp = a[1];
                a[i] = a[i+1];
                a[i+1] = tmp;
            }
        }
    }
}
/* Cercare parole in una lista ordinata:
 * La parola in mezzo è la parola? no, divido a metà
 * Il problema ri riduce logaritmicamente
 */

/* Una stringa è rappresentata dal primo carattere dell'array (per l'ordinamento)
 * minstr
 * Confronto le prime due lettere diverse, altrimenti la più corta
 * se sono uguali è falso
 */

/* T a[] == T* a
 * T* a[]
 * T** a
 *
 * ESEMPIO:
 * int main(int argc, char* argv[])
 *      stampa_stringhe(argv, argc); //STAMPA TUTTO QUELLO CHE GLI PASSI
 *      return 0;
 */
int ltstr(char* a, char* b){
    if (a[0] != b[0]) return a[0] < b[0];
    //COME FARE IL TAIL IN C, SE a è una stringa, allora a è il puntatore al primo carattere, quindi il secondo è a+i
    if(a[0] == "\0") return -1;
    return ltstr(a+1, b+1);
} 
void ordina_stringhe(char* a[], int len){ //Non si possono ordinare le stringhe in c, vanno a indirizzo così --> minstr(char *a, char *b) 
    int i;
    for(int j=0;j<len;j++){
        for(i=0; i<len-1,i++){
            if(ltstr(a[i+1] < a[i])){
                char* tmp[]; //swappa gli elementi
                tmp = a[1];
                a[i] = a[i+1];
                a[i+1] = tmp;
            }
        }
    }
}

void stampa_array_int(int a[], int len){
    for(int i=0;i<len;i++) printf("%i\n", a[i])
}

int main(){
    // Stringhe, parole e sequenze di caratteri
    char a [10] //Non è una stringa
    char *a[10] //È una stringa - array di 10 puntatori a char
    // Ma con quante parole ho a che fare?
    int array0[] = {1,2,3}; Array
    char* a[] = {"ciao", "a", "tutti"};

    stampa_stringhe(a);
    ordina_stringhe(a[], l);
    stampa_stringhe(a);

    /////////
    int b[6] = {1,-25,233,215,73,45};
    stampa_array_int(b, 6);
    ordina_interi(b, 6);
    stampa_array_int(b[6], 6);
}
