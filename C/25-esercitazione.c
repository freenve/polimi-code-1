#include <stdio.h>

/*
 * This file is released under the 3-clause Revised BSD license.
 * Copyright (c) 2015, Francesco Invernici.
 * All rights reserved
 **********************
 * Lezione di lun 14 dic 2015, 14.38.52, CET
 */
//CONCATENAZIONE DI STRINGHE

int len(char *stringa){
    int i = 0;
    while(stringa[i] != '\0') i++;
    return i;
}


void copy(char *origine, char *destinazione){
    int i=0;
    while(origine[i] != '\0'){
    destinazione[i] = origine[i];
    i++;
    }
}

void copy_1(char *origine, char *destinazione){
    int i;
    for(i=0; i<len(origine); i++){
        destinazione[i] = origine[i];
    }
}

char *concat(char *a, char *b){
    printf("######\tAvvio di ConCAT 2015 v3.1 Premium\t#####\n");
    char *c;
    int la, lb, i;
    la = len(a);
    lb = len(b);
    printf("La prima stringa è lunga %d caratte- \t cit. prof. Salvi\n", la);
    printf("La seconda stringa è lunga %d caratte-\t cit. prof. Salvi\n", lb);
    c = malloc(sizeof(char)*(la+lb+1)); //LO SPAZIO ALLOCATO SI CHIAMA HEAP
/*    while(a[i] != '\0'){
        c[i] = a[i];
        i++;
    }
    i = 0;
    while(b[i] != '\0'){
        c[la+i] = b[i];
        i++;
    }*/
    copy(a, c);
//    c[la+1] = ' ';
//    copy(b, (c+sizeof(char)*(la+1)));
    
    copy(b, c+la);
    printf("Il programma è terminato, mister.\n");
    return c;
//    free(c);
} 


int main(){
    char *a = "Anubi";
    char *b = "IlCane";
/*    char s1[LEN];
    char s2[LEN];
    printf("Inserisci una stringa prima di morire:\n");
    scanf("%s", s1);
    printf("Inserisci l'altra stringa, ladro:\n");
    scanf("%s", s2);
    printf("La concatenazione è: %s%s\n",s1,s2);
*/    printf("La funzione restituisce: %s\n",concat(a,b));
//    free(a);
//    free(b);
    }

//Stringa con parole e spazi, vogliamo creare un indice per cercare una parola all'interno del testo, in modo "ottimizzato"
//Struct con 3 campi: parola, lunghezza, indirizzo
