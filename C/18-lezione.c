#include <stdio.h>
/*
This file is released under the 3-clause Revised BSD license.
Copyright (c) 2015, Francesco Invernici.
All rights reserved

Lezione di mar 10 nov 2015, 15.49.03, CET
*/

//Si ritorna a fare funzioni!

int costante() { //Funzione costante
    return 0;
}

int ident(int a) { //Funzione identità
    return a;
    }

int square(int a) {
    return a*a;
    }

int somma(int a, int b) {
    return a + b;
    }

int fibo(int n) {
    if (n < 2){
        return 1;}
    return fibo(n-1) + fibo(n-2);
    }

int isprime(int n){
    int i; //Dichiarazione
    i= 2;   //Assegnazione
    //È possibile inizializare la variabile
    // int i = 2;
    while(i<n){
        if (n % i == 0){
            return 0;
        }
        i = i+1;
    }
    return n;
}
//Cicli FOR
for(i=2, i<n, i++){}


int main(){
    printf("Ciao, mondo! \n"); //Print Formatted
//Primo parametro: Stringa formato --- "%i"
    printf("%i\n", 3);
    printf("%i\n", costante());
    printf("L'identico di 9 è %i\n", ident(9));
    printf("Fibonacci di 4 è %i\n", fibo(4));
    printf("Isprime di 10, 17, 21 sono: %i, %i, %i\n", isprime(10), isprime(17), isprime(21));
}a

/*
isprime in python, versione HARDCORE
def isprime(num):
    return len([x for x in range(2,num) if num % x == 0]) == 0


*/


/*
ESERCIZI:
-DATO UN NUMERO, CALCOLARE LA SOMMA DI TUTTI I DIVISORI DEL NUMERO, ESCLUSO IL NUMERO STESSO

-
*/
