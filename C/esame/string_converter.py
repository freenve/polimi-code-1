#!/usr/bin/env python3
#-*- coding:utf8 -*-
def sostituisci(stringa):
    stringa_ = ""
    for char in stringa:
        if char == 'è' or char == 'é':
            _char = 'e'
        elif char == 'à':
            _char = 'a'
        elif char == 'ò':
            _char = 'o'
        elif char == 'ù':
            _char = 'u'
        elif char == 'ì':
            _char = 'i'
        else:
            _char = char
        stringa_ = stringa_ + _char
    return  stringa_

def prima_maiuscola(stringa):
    return " ".join([word[0].upper()+word[1:] for word in stringa.split()])

def togli(stringa):
    return [char for char in sostituisci(stringa) if 'a'<=char<='z' or 'A'<=char<='Z']

def stringa_ok(stringa):
    new = "".join(togli(prima_maiuscola(stringa)))
    return new

def is_in(lenght):
    return 250 < len(lenght) < 500

if __name__=="__main__":
    from sys import argv
    from os import system
    if len(argv) < 1:
        print('Ho bisogno di una stringa per lavorare...\n')
    else:
        first = str(argv[1])
        res = stringa_ok(first)
        print('La tua stringa era \"' + first + '\"\n')
        print('Ora la tua stringa è \"' + res + '\"\n')
        print('La tua stringa ',end='')
    if is_in(res):
        print('è adatta allo scopo.')
        system("echo '%s' | xclip -sel clip" % res)
    else:
        print('non è adatta allo scopo.')
