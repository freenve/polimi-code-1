#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int is_let(char *c){
    if(('a' <= *c && *c <= 'z') ||
      ('A' <= *c && *c <= 'Z'))
        return 1;
    return 0;
}

int is_voc(char *le){
    if (is_let(le) && (*le=='a' || *le=='i' || *le=='o' || *le=='e' || *le=='u'))
        return 1;
    return 0;
}

int is_cons(char *le){
    if(is_let(le) && !is_voc(le))
        return 1;
    return 0;
}

void *add_minus(char *stringa, int len){
    *(stringa+len) = '-';
    *(stringa+len+1) = '\0';
}

char *the_next(char *pointer){
    return pointer+1;
    }

void *my_cpy(char *stringa, char *from, int delta){
    int i=0;
    while(i<delta){
        stringa[i] = *(from+i);
        i++;
    }
}

char *sillaba(char *stringa){
    int len = strlen(stringa), delta=0, res_len=0;
    char *res = calloc(200, sizeof(char));
    while (*stringa){
    
    if(is_voc(stringa)){
        delta=1;
    }
    if(is_cons(stringa) && is_voc((stringa+1))){
        delta=2;
    }
    if(is_cons(stringa) && is_cons((stringa+1)) && res_len != 0){
        res_len--;
        delta=1;
    }

    if(*stringa == ' '){
        stringa++;
        continue;
    }
    //Alloco lo spazio necessario per la copia
    //DOPO ___ res_len = res_len + delta + 1;
    //Copio le lettere delta
    my_cpy((res+res_len), stringa, delta);
    //Metto il trattino
    res_len = res_len + delta;
    add_minus(res, res_len);
    res_len++;
    //Sposto in avanti la stringa
    stringa=stringa+delta;

    }
    //Tolgo l'ultimo trattino
    //Metto il terminatore
    *(res+res_len-1) = '\0';

    return res;
}

char* copian(char* from, char* to){
    char* res=(char*)malloc(sizeof(char)*(to-from)+1);
    int i;
    for(i=0; i<to-from; i++){
        res[i]=from[i];
    }
    res[i]='\0';
    return res;
}

int isLetter(char c){
    return (c<='z' && c>='a') ||
           (c<='Z' && c>='A') ||
           (c>='0' && c<='9');
}
char *ppp(char *i){
        while(isLetter(*i)) {i++;}
        if(*i == '\0') return NULL;
        while(!isLetter(*i)) {i++;}
        return i;
}
//Accede all'ennesima parola all'interno di una frase e ne ritorna una copia
//assert(strcmp(parola("ciao a tutti"), "tutti") == 1);
char *parola(char *frase, int n){ 
    if(n==0){
        char *i = frase;
        while(isLetter(*i)) {i++;}
        return copian(frase, i);
    }
    else{
        char *i=ppp(frase);
        if(i==NULL) return NULL;
        return parola(i, n-1);
    }
}
int conta_parole(char *word, char *str){
int i=0, j=0;
for(j=0; parola(str,j) != NULL; j++){
        if(strcmp(word, parola(str,j)) ==0)
            i++;
    }
    return i;
}

char *moda(char *frase0){
    char *frase; 
    frase = strdup(sillaba(frase0));
    char *max_str = NULL;
    int max = 0, i;
    for(i=0;parola(frase,i)!=NULL;i++){
        int n = conta_parole(parola(frase, i), frase);
        if(n > max){
             max=n;
             max_str=parola(frase,i);
        }
    }
    return max_str;
}
void main(int argc, char *argv[]){
    printf("%s\n", sillaba(argv[1]));
    printf("%s\n", moda(argv[1]));

}
