#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "sillaba.h"

void main(int argc, char *argv[]){
    char *stringa;
    if (argc<2){
    stringa = strdup("Gimme a string, you moron!");
    }
    else{
    stringa = strdup(argv[1]);
    }
    int len = strlen(stringa);
    printf("########## SILLABATORE SEMPLIFICATO PER L'ITALIANO v0.9b ##########\n");
    printf("Your string was \'%s\' and is %d chars long\n", stringa, len);
    //add_minus(stringa, len);
    printf("Now your string is \'%s\' \n", sillabami(stringa));
    free(stringa);

}
