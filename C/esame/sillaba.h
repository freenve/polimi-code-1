#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int is_let(char *c){
    if(('a' <= *c && *c <= 'z') ||
      ('A' <= *c && *c <= 'Z'))
        return 1;
    return 0;
}

int is_voc(char *le){
    if (is_let(le) && (*le=='a' || *le=='i' || *le=='o' || *le=='e' || *le=='u'))
        return 1;
    return 0;
}

int is_cons(char *le){
    if(is_let(le) && !is_voc(le))
        return 1;
    return 0;
}

void add_minus(char *stringa, int len){
    *(stringa+len) = (char *)calloc(1, sizeof(char));
    *(stringa+len) = '-';
    *(stringa+len+1) = (char *)calloc(1, sizeof(char));
    *(stringa+len+1) = '\0';
}

char *the_next(char *pointer){
    return pointer+1;
    }

void *my_cpy(char *stringa, char *from, int delta){
    int i=0;
    while(i<delta){
        stringa[i] = *(from+i);
        i++;
    }
}

char *first_valid(char *stringa){
    
}

char *sillabami(char *stringa){
    int len = strlen(stringa), delta=0, res_len=0;
    char *res = strdup(" ");
    while (*stringa){
    
    if(is_voc(stringa)){
        delta=1;
    }
    if(is_cons(stringa) && is_voc((stringa+1))){
        delta=2;
    }
    if((*stringa == 'c' || *stringa == 'g') && *(stringa+1)== 'h'){
        delta=3;
    }
    if(is_cons(stringa) && is_cons((stringa+1)) && *(stringa+1) != 'h' && res_len != 0){
        res_len--;
        delta=1;
    }

    if(*stringa == ' ' && res_len!=0){
        *(res+res_len-1) = ' ';
        stringa++;
        continue;
    }
    //Alloco lo spazio necessario per la copia
    *(res+res_len) = (char *)calloc(delta, sizeof(char));
    //DOPO ___ res_len = res_len + delta + 1;
    //Copio le lettere delta
    my_cpy((res+res_len), stringa, delta);
    //Metto il trattino
    res_len = res_len + delta;
    add_minus(res, res_len);
    res_len++;
    //Sposto in avanti la stringa
    stringa=stringa+delta;

    }
    //Tolgo l'ultimo trattino
    //Metto il terminatore
    *(res+res_len-1) = '\0';

    return res;
}

