#include <string.h>
#include <stdio.h>

void main(int argc, char *argv[]){
    char *stringa = strdup(argv[1]);
    char *part;
    int i = 0;
    while(part = strsep(&stringa, "- ")){
        i++;
        printf("%d : %s\n",i, part);
    }
    free(stringa);
    free(part);

}
