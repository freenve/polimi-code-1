#include <stdio.h>

#define DIM (2*2)

void main()
 {
  int m[DIM][DIM];
  int i,s;
  for (i=0;i<DIM*DIM;i++)
  {
      int a=i%DIM, b=i/DIM;
      if(a*b==0)
          m[a][b]=(a+b)%2;
      else
          m[a][b]=m[a-1][b-1];
   }

int r;
int c;
for(r = 0; r < DIM; r++){
    printf("\n");
    for(c = 0; c < DIM; c++){
    printf("%i\t",m[r][c]);
    }
}
}
