#include <stdio.h>
/*
 * This file is released under the 3-clause Revised BSD license.
 * Copyright (c) 2015, Francesco Invernici.
 * All rights reserved
 **********************
 * Lezione di lun 14 dic 2015, 16.27.34, CET
 */

#define MAX 100

typedef struct{
    char *parola;
    int occorrenze;
    char *puntatori[MAX];
} tipo_parola;


int len(char *stringa){
    int i = 0;
    while(stringa[i] != '\0') i++;
    return i;
}

void copy(char *origine, char *destinazione){
    int i;
    for(i=0; i<len(origine); i++){
        destinazione[i] = origine[i];
    }
}


main(){}
