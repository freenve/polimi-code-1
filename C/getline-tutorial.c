#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* utilizzo di getline:
 *
 * int getline(char **string, size_t *bytes, FILE *stream)
 *
 * legge una riga (compresa di \n) dallo stream e aggiorna string e bytes.
 * nel caso string non sia grande abbastanza, getline rialloca memoria
 * sufficente per contenerla (aggiornando il valore di bytes)
 */

int main(){
    // inizializzazione
    size_t bytes = 1;
    //char *mystr = malloc(sizeof(char) * bytes)
    char *mystr = calloc(bytes, sizeof(char));

    //lettura
    printf("type something: ");
    int read = getline(&mystr, &bytes, stdin);

    // gestione errori
    if(read == -1){
        fprintf(stderr, "error reading\n");
        return 1;
    }

    // rimozione \n finale
    char *ret = strchr(mystr, '\n');
    *ret = '\0';

    // output
    printf("you typed: %s (%d bytes)\n", mystr, bytes);

    // finalizzazione
    free(mystr);
    return 0;
}
