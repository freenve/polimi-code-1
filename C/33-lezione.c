#include <stdio.h>
#include <string.h>
#include <stdlib.h>
//#include "32-lezione.c"
/*
 * This file is released under the 3-clause Revised BSD license.
 * Copyright (c) 2015, Francesco Invernici.
 * All rights reserved
 **********************
 * Lezione di mar 19 gen 2016, 16.09.10, CET
 */
//typedef struct _elem elem;
struct _elem {
    char *key;
    int data;
    struct _elem *next;
};

struct _elem *get(struct _elem *d, char *key){
    if(d==NULL) return NULL;
    if(strcmp(d->key, key) == 0) return d;
    return get(d->next, key);
}

void insert(struct _elem **head, char *key, int d){
    struct _elem *obj =(struct _elem *)malloc(sizeof(struct _elem));
    obj->key=strdup(key);
    obj->data=d;
    obj->next=*head;
    (*head)=obj;
}

void printa(struct _elem *head) {
    if(head==NULL) printf("NULL\n");
    else{
        printf("%s: %d ->",head->key, head->data);
        printa(head->next);
    }
}

void inc(struct _elem *head, char *key){
    struct _elem *e = get(head, key);
    if(e) (e->data)++;
    else insert(&head, key, 1);
}

//void libera_tutto(struct _elem **head) {//a+123
//    if(*head == NULL){
//        free(prossimo);
//        return;
//    } 
//    struct _elem *prossimo=(*head)->next;
//    free(*head);
//    libera_tutto(&prossimo);
//}

//a+125
//elem *dict2array(elem *head){
//    int i;
//    elem *j=head;
//    elem *res=(elem *)malloc(sizeof(elem*));
//    for(i=0;i<len
//}


int main(){
    struct _elem *head = NULL;
    insert(&head, "ciao", 10);
    insert(&head, "dunnoh", 19);
    printa(head);
    inc(head, "ciao");
//    struct _elem *obj =malloc(sizeof(struct _elem));
//    obj->key=strdup("cane");
//    obj->data=10;
//    obj->next=head;
//    head=obj;
    insert(&head, "ciao", 10);
//    printf("%s : %d \n", head->key, head->data);
    //insert(head, "dunnoh", 19);
    printa(head);
    free(head);
    return 0;
}
