#!/usr/bin/python
#-*- coding:utf8 -*-

def costante(x):	#Intestazione def -(-):	###Funzione costante
	return 0	#Indentazione obbligatoria

def idem(x):		#Funzione identità
	return x

def succ(x):		#Funzione x+1
	return x+1

def doppio(x):		#Funzione raddopia
	return 2*x

def quadrato(x):	#Funzione quadra
	return x*x

def quadrato_2(x):
	return x**2

def cubo(x):		#Funzione cubo
	return x**3

def poli(x):		#Funzione polinomiale
	return x+2*x**2
def poli_2(x):
	return x+2*quadrato_2(x)

def strana(x):
	return poli(succ(x))
def somma(x, y=0):	#Funzione somma
	return x+y
