##!/usr/bin/env python3
#-*- coding:utf8 -*-
###
#This file is released under the 3-clause Revised BSD license.
#Copyright (c) 2015, Francesco Invernici.
#All rights reserved
###
#Lezione di lun 2 nov 2015, 11.37.54, CET

#Back to the machine!
#Come funzionano i calcolatori e come si interfacciano interpreti e compilatori?

#Automazione dell'informazione
#Leibniz ha inventato il concetto di calcolo infinitesimale, in parallelo a Newton
#Si usavano le tavole logaritmiche e altri strumenti snervanti
#Charles Berber(?) ha creato un sistema meccanico per automatizzare il processo
#I mezzi con cui implementare il calcolo sono stati molteplici, oggi l'elettricità è il mezzo mainstream. In sviluppo ci sono i sistemi di computazione batterica e quantistica

#CONCETTO DI INTERRUTTORE COMANDATO
#Il dispositivo più comune oggi è il transistor
#---PORTE LOGICHE
#---CONCETTO DI FLIP FLOP - 0 e 1
#Intel Nextgen sarà con transistor 3D a 10nm
#Chiuso&chiuso --> si apre, almeno 1 aperto --> si chiude --> PORTA NOR

#COME RAPPRESENTARE L'INFORMAZIONE COME SEQUENZA DI BIT?
#Numeri, immagini, etc
#
#Terminologia: Informazione sullo stato di uno switch, o di un qualsiasi sistema con due possibili stati, è il Bit

#CAMBIAMENTO DI BASE
#BINARIO (0,1)
#Sistema posizionale a potenze di 2
#Rappresentazione delle scelte ad albero. 2^n scelte per il binario
#Quanto sono lunghi i numeri binari fino al 30(10)
#Etichetta lunga 5, disposizioni con ripetizione di 2 oggetti di classe k
#Con un valore di 140 cifre in byte
