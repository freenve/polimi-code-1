#!/usr/bin/env python3
#-*- coding:utf8 -*-
#Esercitazione di Lunedì 12-10-2015, pomeriggio

def doppio(x): 		#Funzione doppia
    return 2*x

def controlla():	#Funzione di controllo, uso di assert
    assert (doppio(0)==0)
    assert (doppio(1)==2)
    assert (doppio(5)==10)
    assert (doppio(10)==20)
    print ("La funzione è corretta") #Funzione print

def doppioB(x):		#Funzione qualsiasi con operatore bitshift
    return ((x+10)>>1

def controlla(f):        #Funzione di controllo per funzioni
    assert (f(0)==0)
    assert (f(1)==2)
    assert (f(5)==10)
    assert (f(10)==20)
    print ("La funzione è corretta")

def test_divisore(f):	#Funzione di controllo per divisore
    assert (f(10, 20) == True)
    assert (f(20, 10) == False)
    assert (f(7, 21) == True)
    assert (f(5, 23) == False)
    print ("La funzione e' corretta")

def divisore(x, y):     #Funzione verifica y divisore di x
    if x > y:
        return False
    if x == y:
        return True
    else:
        return divisore(x,y-x)

def potenza(x, y): #Funzione che eleva x alla y
    if y == 0:
        return 1
    if y == 1:
        return x
    else:
        return x*(potenza(x, y-1,))


def test_potenza(f):
    assert (f(2,5) == 32)
    assert (f(1,10) == 1)
    assert (f(5,2) == 25)
    assert (f(0,10) == 0)
    assert (f(10,0) == 1)
    print ("Funzione corretta")
    
def s(x,y,tmp=0):		#Somma solo con concetto di successivo
   if x<y:
      tmp = y
      y = x
      x = tmp
   if y == 0:
      return x
   return s(x+1,y-1)

def s2(x, y):
   if y == 0:
      return x
   return s(x+1,y-1)

