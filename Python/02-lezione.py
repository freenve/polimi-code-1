#!/usr/bin/env python3
#-*- coding:utf8 -*-
### Lezione di Giovedì pomeriggio 13/10/2015
#
#	KISS --> Portare problema a caso più semplice e risolvere
#
#
###	MCD(a,b) = a, se a == b OR MCD(min(a,b),abs(a-b))	Risoluzione ricorsiva
#
#
#	1) Somma dei primi x numeri
def sommatoria(x):
    return int((x*(x+1))/2)

def sommatoria_1(x):
    return 0 if x == 0 else x + sommatoria(x-1)
#ERGO le funzioni ricorsive richiamano la stessa funzione in ambiente diverso
def sommatoria_2(x):
    i=0
    sum=0
    while i<=x:
        sum = sum + i
        i = i + 1
    return sum
#Tupla - sequenza di valori separati da virgole
t = (2, 5.0, "stringla", ("tupla", 2))
len(t) #Valuta quanti valori contiene la tupla
#Estrarre i valori dalle tuple
t[0] #Count da 0 	Non è possibile cambiare l'ordine dei valori
#Concatenare tuple, ne crea una terza
(1, 2, 3) + (2, 3, 4)

#Somma i valori all'interno di una tupla
def sommatupla(t):
    return t[0] if len(t) == 1 else t[0] + sommatupla(t[1:])

#Per estrarre un intervallo di elementi da una tupla ti usa t[incluso:escluso]

#La tupla con un solo elemento si scrive uno(1, ) con la virgola


#Funzione per generare un intervallo di numeri 0-x in una tupla
def intervallo(x):
    return (0, ) if x == 0 else intervallo(x-1) + (x, )

def intervallo_inverso(x):
    return (x, ) if x/1 == 0 else (x, ) + intervallo_inverso(x-1)

