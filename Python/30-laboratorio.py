#!/usr/bin/env python3
#-*- coding:utf8 -*-
###
#This file is released under the 3-clause Revised BSD license.
#Copyright (c) 2015, Francesco Invernici.
#All rights reserved
###
#Lezione di gio 7 gen 2016, 15.48.49, CET
###GIOCO DEL PERCORSO DI PAROLE
#Dato un file con elenco parole, con una parola per riga, si ritorni un file ordinato
#Paradigmi: Ogni parola è data dall'anagramma di quella precedente o ad un monoedit (aggiunta, rimozione, cambio di lettera)
#Basic: si conoscono inizio e fine
#Advance: Le parole non sono in ordine

def is_anagram(str1, str2):
    ls1 = list(str1).sort()
    ls2 = list(str2).sort()
    return  ls2 == ls1

def is_next(str1, str2):
    ls1 = list(str1).sort()
    ls2 = list(str2).sort()
    len1 = len(ls1)
    len2 = len(ls2)
    diff12 = len([x for x in str1 if x not in str2])
    diff21 = len([y for y in str2 if y not in str1])
    if ls1 == ls2:
        return True
    if  

def solve(word, maze):
    if not maze:
        return;
    endh = open("result.txt", "w")
    zero = len(maze)
    status = 



if __name__ == "__main__":
    from sys import argv
    namefile = argv[1]
    fh = open(namefile, "r")
    words = [line.strip() for line in fh]
    fh.close()
    fh_dest = open("result.txt", "w")


