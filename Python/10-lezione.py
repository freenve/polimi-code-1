#!/usr/bin/env python3
#-*- coding:utf8 -*-
###
#This file is released under the 3-clause Revised BSD license.
#Copyright (c) 2015, Francesco Invernici.
#All rights reserved
###
#Lezione di mar 27 ott 2015, 15.36.04, CET

#Matrici e Python. Come rappresentarle?
#-Insieme di righe-
m = [[1 ,2], [3, 4]]
#Estrarre 1,1:
m[0][0]

#Funzione che restituisce vero se una matrice è una matrice identità
def is_id_2(matrice):
    return matrice == [[1,0],[0,1]]

def trasponi_nuova(matrice): #Solo per matrice 2x2
    nuova_m = [[matrice[0][0], matrice[1][0]], [matrice[0][1], matrice[1][1]]]

def colonna(colonna, matrice): #Restituisce una copia della colonna n-esima di matrice come lista
#    righe = len(matrice)
#    colonne = len(matrice[0])
    nuova_m = []
    for riga in matrice:
        nuova_m = nuova_m + [riga[colonna]]
    return nuova_m

def trasponi(matrice): #Per tutte le matrici quadrate
   colonne = len(matrice[0])
   nuova_m = []
   index = 0
   while index < colonne:
       nuova_m = nuova_m + [colonna(index,matrice)]
       i = i + 1
   return nuova_m

def minimo(lista):
    minor = lista[0]
    for elem in lista:
        if elem < minor:
            minor = elem
    return minor

def minimo_m(matrice):
    minor = minimo(matrice[0])
    for riga in matrice:
        minor_tmp = minimo(riga)
            if minor_tmp < minor:
                minor = minor_tmp
    return minor

def minimo_m_comp(matrice):
    return minimo([minimo(riga) for riga in matrice])

def prodotto_m(matrice1,matrice2):
    nuova_m = []
    i = 0
    while i < len(matrice1):
    

