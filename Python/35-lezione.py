#!/usr/bin/env python3
#-*- coding:utf8 -*-
###
#This file is released under the 3-clause Revised BSD license.
#Copyright (c) 2015, Francesco Invernici.
#All rights reserved
###
#Lezione di mar 26 gen 2016, 15.42.17, CET
#Rudimenti di OOP

class Messaggio:
    def __init__(self, msg): #Metodi
        self.messaggio = msg #Costruttore, prende parametro e lo memorizza nell'oggetto
    def saluta(self):
        print(self.messaggio)

class Persona: 
    def __init__(self, nome):
        self.nome = nome

    def saluta(self):
        print("Ciao, " + self.nome)
    
    def __add__(self, other): #Duck typing
        self.saluta()
        other.saluta()


if __name__ == "__main__":
    f = Persona("Fra") #È come se venisse creato un oggetto di tipo classe = persona
    f.saluta()
