#!/usr/bin/env python3
#-*- coding:utf8 -*-
###
#This file is released under the 3-clause Revised BSD license.
#Copyright (c) 2015, Francesco Invernici.
#All rights reserved
###
#Lezione di lun 26 ott 2015, 15.37.23

def divisors(num):
    return [x for x in list(range(1,num+1)) if num % x == 0]

def divisors_prime(num):
    return [x for x in divisors(num) if divisors(x) == [1, x]]

def exponent(num,factor):
    index = 1
    while num % factor == 0:
        num = num/factor
        index = index + 1
    return (factor, index-1)

def factorization(num):
    primes = divisors_prime(num)
    factors = []
    i = 0
    while len(primes) > 0:
        ex = [exponent(num,primes[0])]
        factors = factors + ex
        primes = primes[1:] 
        i = i + 1
    return factors

def set_lista(lista):
    nu_lista = []
    for elem in lista:
        if elem not in nu_lista:
            nu_lista = nu_lista + [elem]
    return nu_lista

def maggiore_exp(t_0, t_1):
    return t_0[0] == t_1[0] and t_0[1] > t_1[1]

def considera(elem, lista):
    for e in lista:
        if maggiore_exp(e,elem):
            return False
    return True

def mcm_factors(num_1, num_2):
    lista = set(factorization(num_1) + factorization(num_2))
    factors = []
    for elem in lista:
        if considera(elem,lista):
            factors = factors + [elem]
    return factors

def mcm(num_1, num2):
    factors = mcm_factors(num_1,num_2)
    num = 1
    for factor in factors:
       num = factor[0]**factor[1] * num
    return num

if __name__ == "__main__":
    print("Calcolatore di Minimo Comune Multiplo tra due numeri - VER 2.00")
    num_1 = int(input("Immettere il primo numero: "))
    num_2 = int(input("Immettere il secondo numero: "))
    print(mcm(num_1, num_2))

#Scrivere una funzione che data una lista restituisca l'elemento minore
def gen():
    return [randint(-100,100) for x in range(0,10)]

def minore_in_lista(lista):
    minor = lista[0]
    for elem in lista:
        if elem < minor:
            minor = elem
    return minor

def minore_rec(lista):
    if len(lista) == 1:
        return lista[0]
    m = minore_rec(lista[1:])
    if m > lista[0]:
        return lista[0]
    return m

#timeit(valuta) restituisce il tempo impiegato per loop, ottimo per le analisi di ottimizzazione
