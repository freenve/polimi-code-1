#!/usr/bin/env python3
#-*- coding:utf8 -*-
###
#This file is released under the 3-clause Revised BSD license.
#Copyright (c) 2015, Francesco Invernici.
#All rights reserved
###
#Lezione pomeridiana di martedì 20/10/2015

#Funzioni ricorsive generano frame (ambienti) in background, cicli si mangentono all'interno dello stesso spazio

#I valori sono di diversi diversi tipi, che sono utili per scopi diversificati:
#Le caratteristiche specifiche e gli operatori hanno effetti diversi
#Intero
#Virgola mobile
#Lista - Lunghezza, accesso ad un elemento, modifica di un elemento
#Tupla - Lunghezza, accesso ad un elemento
#Stringa - "testo" or 'testo' - l'apice singono in print indica l'elaborazione - Leggere la Biblioteca di Babele, Borges
#Literal - Rappresentazioni testuali di valore. 1 è una rappresentazione testuale del numero 1. Es, Gli input sono rappresentazioni testuali di valori, non valori propri

#Concatenare stringhe come con liste e altre operazioni tipiche con sequenze, come lunghezza, contare

def add(a, b):     #La chiamata a funzione è solo un modo per operare su dei valori, ci sono altri metodi. È solo un formalismo scelto.
    return a + b

esempio = "Ciao a tutti"
#Immaginare una funzione che da una stringa ritorni una lista di stringhe che rappresentano le parole
#In python è builtin, splitta al delimitatore passato come parametro.
esempio.split(" ")		#valore.funzione(altro parametro) - non una funzione, ma metodo

def split_f(n, delimitatore):
    return n.split(delimitatore)		#Split è un METODO del tipo di valore stringa.

#Nelle varie documentazioni è indicato come richiamare le funzioni


def parola_in_testo(parola,testo):
    lista = testo.split(" ")
    return True if sum(list(map(lambda x: True if x == parola else False, lista))) > 0 else False

def parola_in_testo_w(parola,testo):
    lista = testo.split(" ")
    l = len(lista)
    index = 0
    while (index < l):
        if lista[index] == parola:
            return True
        index = index + 1
    return False
