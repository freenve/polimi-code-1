#!/usr/bin/env python3
def divisori(num):
    return [x for x in range(0,num+1) if num%x == 0]
def misura(num):
    def divisori(num):
        return [x for x in range(0,num+1) if num%x == 0]
    return 0 if sum(divisori(num)) == num else (num - sum(divisori(num)))

#IN C
int misura(n){
    int sum;
    sum = 0;
    int i;
    for(i=1;i<n;i++){
        if (n%i == 0) sum = sum + i;
    }
    if(sum == n) return 0;
    if(sum < n) return -1;
    else return 1;
    
}
