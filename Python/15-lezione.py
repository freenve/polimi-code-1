#!/usr/bin/env python3
#-*- coding:utf8 -*-
###
#This file is released under the 3-clause Revised BSD license.
#Copyright (c) 2015, Francesco Invernici.
#All rights reserved
###
#Lezione di mar 3 nov 2015, 16.08.34, CET

#Scrivi una funzione che scrive il complemento

def listino(num):
    return [int(n) for n in [n for n in str(num)]]

def complementa(num):
    _num = listino(num)
    return [1-n for n in _num]

def somma_bit(b1,b2):      #Questo circuito si chiama HALF ADDER - SUCH WOW
    if b1 == 0 and b2 == 0: return [0, 0]
    if b1 == 0 and b2 == 1: return [0, 1]
    if b1 == 1 and b2 == 0: return [0, 1]
    return [1,0]

def full_adder(b1,b2,b3):    #Bit1,Bit2,Riporto
    s = b1 + b2 + b3
    if s == 0: return [0, 0]
    if s == 1: return [0, 1]
    if s == 2: return [1, 0]
    if s == 3: return [1, 1]

def somma_1(num) #Questa funzione somma 1 a qualsiasi bit
    _num = listino(num)
    s = [0, 0 ,0]
    s_tmp = somma_bit(_num[1],1)
    s[2] = s_tmp[1]
    s_tmp2 = sommabit(num[0], s_tmp[0])
    s[1] = s_tmp[1]
    s[0] = s_tmp2[0]
    return s

#Leggibilità e generalizzare per n qualsiasi
