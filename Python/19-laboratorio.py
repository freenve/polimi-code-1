#!/usr/bin/env python3
#-*- coding:utf8 -*-
###
#This file is released under the 3-clause Revised BSD license.
#Copyright (c) 2015, Francesco Invernici.
#All rights reserved
###
#Lezione di gio 12 nov 2015, 15.42.44, CET


def rnd_move(match, player):
    done = 0
    while done == 0:
        a = my_random()
        b = my_random()
        if match[a][b] == 0:
            match[a][b] = player
            done = 1
            return done
    return 0

def is_full(match):
    for line in match:
        for el in line:
            if el == 0:
                return False
    return True

def is_full_0(match):
    for line in match:
        if 0 in line:
            return False
    return True

def move(row, line, player, match):
    if match[row][line] == 0:
        match[row][line] = player
        return 1
    return 0

def auto_move(player,match):
    if not is_full_0:
        return 0
    else: 
        done = 0
        while done == 0:
            a = my_random()
            b = my_random()
            if match[a][b] == 0:
                match[a][b] = player
                done = 1
                return done

###
#Gen board
#Random player starter
#Mossa 1
#Cambio giocatore con contatore
#Il contatore fa 3 meno la somma di quello prima x = 3 - x
#Finché la cosa non è piena o qualcuno vince
#Fine vinto 1, vinto 2, parità

#SCRIVERE FUNZIONE CHE FACCIA MOSSA NELLA PRIMA POSIZIONE LIBERA
#FARE FUNZIONE CHE CONTROLLI CHE TRA VECCHIA MOSSA E NUOVA MOSSA SIA CAMBIATA SOLO UNA POSIZIONE, SE IMBROGLIA SQUALIFICA E FA VINCERE ALTRO.
#Puù giocatore n andare da matrice 0 a 1 con una mossa valida? (Imbroglio qualsiasi/Sovrascrittura)

def sim_match():
    _board = board(3)
    player = randint(1,2)
    while not is_win_match(_board) and not is_full(_board):
        auto_move(player,_board)
        player = 3 - player
    status_win = is_win_match(_board)
    status_full = is_full(_board)
    print("Match is ", status_win, "and board is ", status_full)
    pretty_print(_board)
    return 1
