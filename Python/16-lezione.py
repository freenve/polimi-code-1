#!/usr/bin/env python3
#-*- coding:utf8 -*-
###
#This file is released under the 3-clause Revised BSD license.
#Copyright (c) 2015, Francesco Invernici.
#All rights reserved
###
#Lezione di ven 6 nov 2015, 11.52.05, CET

#Warm up!
def neper(n):
    return (1+(1/n)**n

def epsilon(val, function): #Function must be increasing
    i = 1
    while function(i+1) - function(i) > val:
        i = i +1
    return i-1

def inverti_bit(lista):
    l = []
    while len(lista) >= 0:
        l = l + [lista[0]]
        lista = lista[0:]
    return l

def somma1_6(n):
    s = n[0] + 1
    somma_b_ms = s % 2 #Tecnica per ottenere il lsb
    riporto = s // 2 #Tecnica per ottenere il msb da un bin a 2 cifre
    return [somma_b_ms] + somma1_5(n[1:],riporto)

def somma_bit(n,b):
    s = n[0] + b
    somma_b_ms == s % 2
    riporto = s // 2
    if len(n) == 1:
        return [somma_b_ms, riporto]
    return [somma_b_ms] + somma(n[1:], riporto)

def somma_1_iter(n):
    riporto = 1
    for i in range(len(n)):
        s = riporto + n[i]
        n[i] = s % 2
        riporto = s // 2
    n = n + [riporto]
    return n

def somma_a_b(a, b):

def inverti(lista):
    n_lista = []
    for i in lista:
        n_lista = [lista[0]] + n_lista
        lista = lista[1:]
    return n_lista
 
def to_list(val):
    stringa = str(val)
    lista = []
    for i in stringa:
        lista = lista + [stringa[0]]
        stringa = stringa[1:]
    return lista
 
def to_list1(val):
    return [int(x) for x in str(val)]
 
def somma(b0,b1):
    l0 = inverti(to_list1(b0))
    l1 = inverti(to_list1(b1))
    if len(l1) > len(l0):
        l0 = l0 + [0]*(len(l1)-len(l0))
    else:
        l1 = l1 + [0]*(len(l0)-len(l1))
    b_s = [0]*len(l0)
    riporto = 0
    for el in list(range(0,len(b_s))):
        b_s[el] = (l0[el]+ l1[el] + riporto) % 2
        riporto = 1 if (l0[el] + l1[el] + riporto) >= 2 else 0
    if riporto == 1:
        b_s = b_s + [1]
    return inverti(b_s)
