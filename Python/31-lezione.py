#!/usr/bin/env python3
#-*- coding:utf8 -*-
###
#This file is released under the 3-clause Revised BSD license.
#Copyright (c) 2015, Francesco Invernici.
#All rights reserved
###
#Lezione di ven 8 gen 2016, 11.34.01, CET

#Siamo partiti cercando la parola più frequente in un testo, ora introduciamo i dizionari
#Si possono associare indici a parole, tipo le struct in c
def frequencies(words):
    fs = {}
    for w in words:
        if w in fs:
            fs[w] += 1
        else:
            fs[w] = 1
    return fs

def max_value(d, esclusi):
    max = 0
    for k in d.keys():
        if not in esclusi and d[k] > max:
            max = d[k]
            max_key = k
    return max_key

def dict_to_couples(di):
    cs = []
    for k in di.keys():
        cs += [(di[k], k)]
    return cs
