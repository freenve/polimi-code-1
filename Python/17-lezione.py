#!/usr/bin/env python3
#-*- coding:utf8 -*-
###
#This file is released under the 3-clause Revised BSD license.
#Copyright (c) 2015, Francesco Invernici.
#All rights reserved
###
#Lezione di lun 9 nov 2015, 11.35.35, CET

#RANSOM - Hashtag di oggi ;) (Riscatto, per gli Italioti come me)

#CODIFICA ASCII
#Codifichiamo i simboli testuali in numeri
#Tabella ascii - 7bit + 1 di simbolo
#a = 65 - 0100 0001
#b = 66 - 0100 0010

#I file sono "nastri di bite" divisi per 8 bit
#Estendiamo a 16 bit
#MSB - Se = 0 è un carattere ASCII, se 1 è SUPER_ASCII

#FILE HANDLER - Oggetto di manipolazione all'interno di un file
#Testina magnetica costantemente posizionata su un bite qualsiasi.
#All'inizio è sul bite 0, all'inizio
#In python è read
#Write scrive
#Seek sposta la testina
#Tell ritorna dov'è la testina

#Vedi 17-lezione.txt per il file considerato

fh = open("17-lezione.txt") # File Handler
fh.read(1) #Legge un carattere, la testina si è spostata di uno
fh.tell() #Posizione della testina
fh.seek(0) #Spostati in posizione 0
fh.write("Stringa") #Sovrascrive
fh.close() #Chiude il file
#Open prende altri parametri, tra cui i permessi 
#w è overwrite
#r+ è read extended
fh_0 = open("17-lezione.txt", "r+")

#Python scrive su buffer, non direttamente sul file
#Solo al momento del close scrive sul file

#Scrivere una funzione cifrario di cesare che da un carattere ne ritorni il carattere corrispondente cifrato.

def cc(c):
    if c == "z":
        return "a"
    elif c == "Z":
        return "A"
    elif c >= "a" and c <= "z":
        return chr(ord(c)+1)
    else:
        return c

def cc_string(stringa):
    return list(map(cc, stringa))

def cc_inv(c):
    if c == "a":
        return "z"
    elif c == "A":
        return "Z"
    return chr(ord(c)-1)

def crypt(nomefile): #Questa funzione cripta solo il primo carattere
    fh = open(nomefile, "r+")
    c = fh.read(1)
    fh.seek(fh.tell() - 1)
    fh.write(cc(c))
    fh.close()

def crypt_long(nomefile):
    fh = open(nomefile, "r+")
    while True:
        c = fh.read(1)
        if c == "":
            fh.close()
            return
        fh.seek(fh.tell() - 1)
        fh.write(cc(c))
