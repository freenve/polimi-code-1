#!/usr/bin/env python3
#-*- coding:utf8 -*-
###
#This file is released under the 3-clause Revised BSD license.
#Copyright (c) 2015, Francesco Invernici.
#All rights reserved
###
#Lezione di lunedì 19/10/2015,mattina

#RIP - Mappare una sequenza su una funzione
#map(funzione,lista) --> lista con valori mappati
def square(x): #Eleva al quadrato
    def x**2

list(map(square, [1,2,3]))

#GENERATOR EXPRESSION di PYTHON
[x**2 for x in [1,2,3]]
#x**2 - map expression
[square(x) for x in [1,2,3]]

from random import randint #Generatore di intero casuale tra intervallo

#Espressione di FILTRO nel gen expression - posteriore a statement
lista = [randint(1,10) for x in [1, ]*30]
[1 for x in lista if x == 10 or x == 1]

#ADVANCED - Se in tupla, in python 3 ritorna un generatore,ovvero, una promessa di generare una lista, non la lista

#Sequenza di Fibonacci
#1 1 2 3 5 8 13 21 44
#f(1) = 1, f(n) = f(n-1)+f(n-2)
def fibo(n):			#Con funzione ricorsiva è un po' una merda, troppo da valutare
    if (n<2):			#Costrutto condizionale
        return 1
    else:
        return fibo(n-1)+fibo(n-2)

def fibo_c(i,fi,fi_1,n):	#Risolto il problema della doppia ricorsione, WLF
    if i == n:			#Albero delle chiamate è lineare, se raddoppio richiesta, raddoppia il tempo
       return fi
    else:
       return fibo_c(i+1,fi+fi_1,fi,n)

def fibo_0(n,fi=1,fi_1=1,i=0):
    while i <= n:
        fi = fi + fi_1
        fi_1 = fi
        i = 1 + i
    return fi
