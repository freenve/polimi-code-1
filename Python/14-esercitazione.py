#!/usr/bin/env python3
#-*- coding:utf8 -*-
###
#This file is released under the 3-clause Revised BSD license.
#Copyright (c) 2015, Francesco Invernici.
#All rights reserved
###
#Lezione di lun 2 nov 2015, 14.39.33, CET

def convert(number,base):
    _number = ""
    while number > 0:
        _number = str(number % base) + _number
        number = number // base
    return int(_number)

def listbin(num):
    number = []
    while num > 0:
        number = [num % 2] + number
        num = num // 2
    return number
