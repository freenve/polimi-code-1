#!/usr/bin/env python3
#-*- coding:utf8 -*-
###
#This file is released under the 3-clause Revised BSD license.
#Copyright (c) 2015, Francesco Invernici.
#All rights reserved
###
#Lezione di gio 3 dic 2015, 15.36.54, CET

#SOLO FUNZIONE MOSSA PER VINCERE

def column_MIM(board, n):
    return [line[n] for line in board]

def row_MIM(board, n):
    return board[n]

def diag_MIM(board, name):
    if name == 0:
        return [board[i][i] for i in range(0,len(board))]
    if name == 1:
        return [board[len(board)-i-1][i] for i in range(0,len(board))]

def is_empty(board):
    for line in board:
        for elem in line:
            if elem != 0:
                return False
    return True

def zeros(board):
    somma = 0
    for line in board:
        somma = somma + sum(item == 0 for item in line)
    return somma

def play(board, player):
    if player == 1:
        ###CHECKWIN
        ###GOWIN
        ###GOBLOCK
        if zeros(board) == 9:
            return (0,0)
        if zeros(board) == 7 and ((board[1][0] == 3-player) or (board[2][0] == 3-player) or (board[2][1] == 3-player) or (board[2][2] == 3-player)):
            return (0,2) ##A
        if zeros(board) == 5 and board[0][1] == 3-player and board[2][1] == 3-player:
            return (1,1) ##AB --> win
        if zeros(board) == 5 and board[0][1] == 3-player:
            return (2,2) ##AC --> win
        if zeros(board) == 7 and ((board[0][1] == 3-player) or (board[0][2] == 3-player) or (board[1][2] == 3-player) or (board[2][2] == 3-player)):
            return (2,0) ##A
        if zeros(board) == 5 and board[1][0] == 3-player and board[1][2] == 3-player:
            return (1,1) ##AB --> win
        if zeros(board) == 5 and board[1][0] == 3-player:
            return (2,2) ##AC --> win
        if zeros(board) == 7 and board[1][1] == 3-player:
            return (2,2)
        if zeros(board) == 5 and board[0][2] == 3-player:
            return (2,0)
        if zeros(board) == 5 and board[2][0] == 3-player:
            return (0,2)
    if player == 2:
        if zeros(board) == 8 and board[1][1] == 3-player:
            return (0,0)
        if zeros(board) == 8 and board[0][1] == 3-player:
            return (1,1)
        if zeros(board) == 8 and board[0][0] == 3-player:
            return (1,1)
        if zeros(board) == 6 and board[0][0] == 3-player and board[2][2] == 3-player:
            return (0,2)
        r = 0
        c = 0
        while r < 3:
            while c < 3:
                if board[r][c] == 0:
                    return (r,c)
                c = c + 1
            c = 0
            r = r + 1

def win_line(line, player):
    presence = sum(elem = player for elem in line)
    if presence == 2:
        

def go_win_MIM(match, player):
    
    columns = [column(match,n) for n in range(0,len(match))]
    diags = [diag(match,n) for n in range(0,2)]
    return len(set(row_vict + col_vict + diag_vict)) > 1
