#!/usr/bin/env python3
###WORD MAZE SOLVER

def is_next(elem1, elem2):
    if elem1 == elem2-1:
        return True
    else:
        return False

def read_words(filename):
    fh = open(filename, 'r')
    words_list = [word.strip('\n') for word in fh.readlines()]
    fh.close()
    return words_list

def extract_first(li):
    return li[0]

def extract_last(li):
    return li[-1]

def is_solved(tmp_list, fin_len, last):
    return (len(tmp_list) is fin_len) and (last is extract_last(tmp_list))

def the_next(word_list, solved):
    i = 0
    lenght = len(word_list)
    while i < lenght:
        if is_next(solved[-1], word_list[i]):
            return i
        i += 1
    return None

def solve(word_list, solved, total, last): #word_list deve essere passata sortata (?) e senza la prima parola
    if is_solved(solved, total, last):
        return solved
    elif the_next(word_list, solved) != None:
        print(word_list[the_next(word_list, solved)])
        solved.append(word_list[the_next(word_list, solved)])
        del word_list[the_next(word_list, solved)]
    else:
        word_list.append(solved[-1])
        del solved[-1]
    print(solved)
    return solve(word_list, solved, total, last)

if __name__ == "__main__":
    from sys import argv
    word_list = read_words(argv[1])
    print(word_list)
    first = extract_first(word_list)
    print(first)
    last = extract_last(word_list)
    print(last)
    total_len = len(word_list)
    print(total_len)
    solved = [first]
    print(solved)
    del word_list[0]
    solve(word_list, solved, total_len, last)
