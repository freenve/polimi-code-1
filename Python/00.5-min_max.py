#!/usr/bin/env python3
#-*- coding:utf8 -*-
###
#This file is released under the 3-clause Revised BSD license.
#Copyright (c) 2015, Francesco Invernici.
#All rights reserved
###

def max(x, y):
    return ((x+y)+((x-y)**2)**(1/2))/2

def min(x, y):
    return ((x+y)-((x-y)**2)**(1/2))/2

#while input("Maggiore o minore? Premi invio per uscire"):

print("Maggiore o minore?")
a = eval(input('Inserisci il primo numero da valutare: '))
b = eval(input('Inserisci il secondo numero da valutare: '))
print("Il maggiore è ",max(a, b),", il minore è ",min(a, b))
