#!/usr/bin/env python3
#-*- coding:utf8 -*-
###
#This file is released under the 3-clause Revised BSD license.
#Copyright (c) 2015, Francesco Invernici.
#All rights reserved
###
#Esercitazione di lunedì 19/10/2015
#Gioco - Ci sono 3 pacchi, in uno dei tre pacchi c'è un premio, il giocatore sceglie un pacco, il presentatore apre uno dei pacchi rimasto senza premio.
#Calcolare la probabilità di vincere
from random import randint
def box(n=3):
    lista = [0, ] * n
    index = randint(0,n-1)
    lista[index] = 1
    return lista

def partita(n=3): #Propagazione del numero di pacchi
    p = box(n)
    scelta = randint(0,n-1)					#Pacco scelto
    pacco_A = scelta - 1 if scelta - 1 >= 0 else scelta-1+n	#Pacchi rimanenti
    pacco_B = scelta - 2 if scelta - 2 >= 0 else scelta-2+n
    if p[pacco_A] == 0:
       return 1 - p[pacco_B]
    return 0

def partite(times):
    partite = [3, ]*times
    return sum(list(map(partita,partite)))*100/times

#I CICLI
def stampa_ciao(x):
    return "ciao"
lista = [0, ]*10
list(map(stampa_ciao, lista))

#IL CICLO WHILE (statement) - FINCHÉ
i = 0
while(i<10):
    print("ciao ",i)

#Se il numero di ripetizioni è finito, stabile e logico come funzione applicata a valore, map è una buona soluzione. Altrimenti while.
def fibo(n):
    fib_0 = 1
    fib_1 = 1
    if n < 3:
        return 1
    while(n>=3):
        fib_2 = fib_0 + fib_1
        fib_0 = fib_1
        fib_1 = fib_2
        n = n-1
    return fib_2

def fibo_step(num):
    fib_0 = 1
    fib_1 = 1
    fib_2 = 1
    n = 0
    if num < 2:
        return 1
    while(fib_2<num):
        fib_2 = fib_0 + fib_1
        fib_0 = fib_1
        fib_1 = fib_2
        n = n+1
    return n

def zip_again(list_1, list_2):
    i = 0
    lista = []
    while (i <  len(lista_1) or 1 < len(lista_2)):
        lista = lista + [(list_1[i] + list_2[i])]
        i = i+1
    return list

def backzip(list_1, list_2):
    i = min(len(list_1),len(list_2)) 
    lista = []
    while (i >= 0):
        lista = lista + [(list_1[-1] + list_2[-1])]
        i = i-1
    return lista

#SCRIVERE ZIP CHE UNISCE PRIMO DI PRIMA LISTA E ULTIMO DI SECONDA LISTA
