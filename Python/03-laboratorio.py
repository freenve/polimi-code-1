#!/usr/bin/env python3
#-*- coding:utf8 -*-
###
#This file is released under the 3-clause Revised BSD license.
#Copyright (c) 2015, Francesco Invernici.
#All rights reserved
###
#Laboratorio di giovedì 15/10/2015

from random import shuffle

def my_map(function,lista):
    return list(map(function,lista))

def deck(): #Ritorna un mazzo mischiato e zippato con il controllo
    controllo_carte = (1, 2, 3) * 13 + (1, )
    carte = (list(range(1,11)))*4
    shuffle(carte)
    return list(zip(carte,controllo_carte))

def is_equal(lista): #Quando sono uguali, hai perso e ritorna 1
    return 1 if lista[0] == lista[1] else 0

def is_won():
    case = sum(set(my_map(is_equal,deck())))
    return 1 if case == 0 else 0

def percent(times):
    def chance(times):
        return 0 if not times else is_won()+chance(times-1)
    fav = chance(times)
    return fav*100/times

if __name__ == "__main__":
    from time import sleep
    print("Simulatore statistico del gioco delle tre carte consecutive")
    volte =  int(input("Quante volte vuoi tentare la sorte? "))
    print("Complimenti! Hai vinto lo " + str(percent(volte)) + "% delle volte.")
#    sleep(10)
