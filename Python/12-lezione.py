#!/usr/bin/env python3
#-*- coding:utf8 -*-
###
#This file is released under the 3-clause Revised BSD license.
#Copyright (c) 2015, Francesco Invernici.
#All rights reserved
###
#Lezione di ven 30 ott 2015, 11.00.44, CET

def pretty_print(riga):
    for el in riga:
        print(str(el),end = "\t")
    return 0

def pretty_print_m(matrix):
    for riga in matrix:
        pretty_print(riga)
        print("\n")
    return 0

def riga(matrice, n):
    return matrice[n]

def colonna(matrice, n):
    return [riga[n] for riga in matrice]

def prod_vect(vect_1,vect_2):
    return sum([vect_1[n-1] * vect_2[n-1] for n in vect_2])

def pm_point(m,n,i,j):
    return prod_vect(riga(m,i),colonna(n,j))

def gen_m(righe,colonne):
    return [[0]*colonne]*righe

def pm(m,n):
    lines = len(m)
    columns = len(n[0])
    i = 0
    j = 0
    matrix = gen_m(lines,columns)
    while i < lines:
        while j < columns:
            matrix[i][j] = pm_point(m,n,i,j)
            j = j + 1
        i = i + 1
    return matrix
#NON FUNZIONANO, GLI INDICI SONO LISTE, DEVONO ESSERE INTERI 
def pm_for(m,n):
    matrix = gen_m(len(m), len(n[0]))
    for line in m:
        for column in n:
            matrix[line][column] = pm_point(m,n,line,column)
    return matrix

def pm_oneline(m,n):
    matrix = gen_m(len(m), len(n[0]))
    return  [[pm_point(m,n,line,column) for column in line] for line in matrix]
