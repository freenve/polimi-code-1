#!/usr/bin/env python3
#-*- coding:utf8 -*-
###
#This file is released under the 3-clause Revised BSD license.
#Copyright (c) 2015, Francesco Invernici.
#All rights reserved
###
#Lezione di Venerdì 16/10/2015

###Compiti - Prodotto scalare tra due vettori a n dimensioni - Funzione zip costruita come map
def sqt(t): #Restituisce i quadrati dei valori contenuti in una lista
    return [] if len(t)==0 else [t[0]**2] + sqt(t[1:])

def my_map(function,list):###Funzione che riceve come parametro una funzione da applicare a una lista, ossia map( , )
    return [] if len(list)==0 else [function(list[0])] + my_map(function,list[1:]) 

#Posso risolvere una ricerca come una funzione di conta - Applicando una funzione somma, ritorna il numero di presenze nella lista
def search(value, lista):
    return [] if len(lista) == 0 else [True, ] + search(value, lista[1:]) if lista[0] == value else [False, ] + search(value, lista[1:])

#sum(search(value,lista))

def _is3(n):  #Predicato - Funzione che verifica un valore
    return 1 if n == 3 else 0

def somma_tupla(tupla): #Operazione di riduzione su una lista - reduce su una lista usando il somma
    return 0 if len(tupla)==0 else tupla[0] + somma_tupla(tupla[1:])

def prodotto_tupla(tupla): #Operazione di riduzione su una lista - reduce su una lista usando il prodotto
    return 1 if len(tupla)==0 else tupla[0] * prodotto_tupla(tupla[1:])

### Con map e un particolare reduce sono stati sviluppati sistemi informativi ottimizzati per tale scopo

#Spiegazione di ZIP
def my_zip(tupla1,tupla2):
    return [] if not tupla1 or not tupla2 else [((tupla1[0], ) + (tupla2[0], ))] + my_zip(tupla1[1:],tupla2[1:])

#Prodotto scalare di due vettori
def scalar_vect(tupla1,tupla2):
    lista = list(zip(tupla1,tupla2))
    return 0 if not lista else lista[0][0]*lista[0][1] + scalar_vect(tupla1[1:],tupla2[1:])
    
def scalar_vect_m(tupla1,tupla2):
    lista = list(zip(tupla1, tupla2))
    return sum(list(map(lambda x: x[0]*x[1],lista)))
