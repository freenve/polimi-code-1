#!/usr/bin/env python3
#-*- coding:utf8 -*-
###
#This file is released under the 3-clause Revised BSD license.
#Copyright (c) 2015, Francesco Invernici.
#All rights reserved
###

#Oggi si gioca a TIC TAC TOE!

from random import randint

def board(n):
    board_0 = [[0]*n for _ in range(0,n)]
    for line in board_0:
	    print(line)
    return board_0

def my_random():
    return randint(0,2)

def ended_match(n):
    board = [[my_random() for x in range(0,n)] for x in range(0,n)]
    for line in board:
        print(line)
    return board

def column(board, n):
    return [line[n] for line in board]

def row(board, n):
    return board[n]

def diag(board, name):
    if name == 0:
        return [board[i][i] for i in range(0,len(board))]
    if name == 1:
        return [board[len(board)-i-1][i] for i in range(0,len(board))]

def is_win_line(line):
    this = line[0]
    for elem in line: ###LO ZERO NON DEVE VINCEREEEE
        if elem !=  this and elem != 0:
            return 0
    return 1

def pretty_print(board):
    for line in board:
        print(line)
    return 0

def check_set(line):
    return 1 if len(set(line)) == 1 else 0

#RIFARE CON CHECK SET E CONTROLLARE LUNGHEZZA DEL SET

def is_win_match(match):
    row_vict = [is_win_line(row) for row in match]
    columns = [column(match,n) for n in range(0,len(match))]
    col_vict = [is_win_line(col) for col in columns]
    diags = [diag(match,n) for n in range(0,2)]
    diag_vict = [is_win_line(diago) for diago in diags]
    return len(set(row_vict + col_vict + diag_vict)) > 1

#DA FARE: CONTROLLO SU TUTTE E FUNZIONE CHE FACCIA UNA MOSSA QUALSIASI DATA UNA SCACCHIERA

def rnd_move(match, player):
    done = 0
    while done == 0:
        a = my_random()
        b = my_random()
        if match[a][b] == 0:
            match[a][b] = player
            done = 1
            return done
    return 0

def is_full(match):
    for line in match:
        if 0 in line:
            return False
    return True

def move(row, line, player, match):
    if match[row][line] == 0:
        match[row][line] = player
        return 1
    return 0

def auto_move(player,match):
    if not is_full_0:
        return 0
    else: 
        done = 0
        while done == 0:
            a = my_random()
            b = my_random()
            if match[a][b] == 0:
                match[a][b] = player
                done = 1
     return done

def sim_match_0():          #RANDOM CONTRO RANDOM
    _board = board(3)
    player = randint(1,2)
    while not is_win_match(_board) and not is_full(_board):
        auto_move(player,_board)
        player = 3 - player
    status_win = is_win_match(_board)
    status_full = is_full(_board)
    print("Match is ", status_win, "and board is ", status_full)
    pretty_print(_board)
    return (3 - player) if status_win else 0

def first_free_move(player, match):
    if not is_full(match):
        return 0
    else:
        done = 0
        for line in match:
            for el in line:
                if done == 0:
                    if el == 0:
                        el = player
                        done = 1
                        return done
        return is_full(match)

def diff(zipped_list):
    return sum([0 if x[0] == x[1] else 1 for x in zipped_list])

def check_valid(board_0, board_1):
    diffs = 0
    zipped = list(zip(board_0,board_1))
    for line in zipped:
        diffs = diffs + diff(line)
    return True if diffs <= 1 else False
