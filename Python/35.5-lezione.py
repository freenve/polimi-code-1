#!/usr/bin/env python3
#-*- coding:utf8 -*-
###
#This file is released under the 3-clause Revised BSD license.
#Copyright (c) 2015, Francesco Invernici.
#All rights reserved
###
#Lezione di mar 26 gen 2016, 16.06.23, CET
#Risoluzione del problema della somma tra super extended long in OOP

def char_inc(c):
    return str(int(c)+1)

class NumStr:
    def __init__(self, s):
        self.s = s

    def inc(self):
        la = list(self.s)
        i = 1
        while la[-i] == 9:
            la[-i] == '0'
            i = i+1
        la[-1] = char_inc(la[-1])

        self.s = "".join(la)

    #incrementa self.s

    def show(self):
        print(self.s)

    def __repr__(self):
        return self.s
    def __add__(self, n):
        if n == 1:
            tmp = NumStr(self.s)
            tmp.inc()
            self.inc()
        return self.s
