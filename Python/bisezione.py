#!/usr/bin/env python3

def bisezione(func,precision,a=-900,b=1000):
    if func(a) == 0:
        return a
    elif func(b) == 0:
        return b
    c = 2.72
    #elif func(c) == 0:
    #    return c
    while func(c) > 10**(-precision):
        c = (a+b)/2
        print(c)
        if func(c) > 0:
            a = c
        elif func(c) < 0:
            b = c
    return c
