#!/usr/bin/env python3
#-*- coding:utf8 -*-
###
#This file is released under the 3-clause Revised BSD license.
#Copyright (c) 2015, Francesco Invernici.
#All rights reserved
###
#Lezione di venerdì 23/10/2015

def divisors(num):
    return [x for x in list(range(1,num+1)) if num % x == 0]

def divisors_prime(num):
    return [x for x in divisors(num) if divisors(x) == [1, x]]

def exponent(num,factor):
    index = 1
    while num % factor == 0:
        num = num/factor
        index = index + 1
    return (factor, index-1)

def factorization(num):
    primes = divisors_prime(num)
    factors = []
    i = 0
    while len(primes) > 0:
        ex = [exponent(num,primes[0])]
        factors = factors + ex
        primes = primes[1:] 
        i = i + 1
    return factors
    
def right_factor(num, lista):
    while len(lista) > 0:
        if num[0] == lista[0][0]:
            return (num[0], max(num[1],lista[0][1]))
        else:
            lista = lista[1:]
    return num

def mcm_factors(num_1, num_2):
    factors_1 = factorization(num_1)
    factors_2 = factorization(num_2)
    factors_a = []
    factors_b = []
    for num in factors_1:
        factors_a = [right_factor(num,factors_2)] + factors_a
    for num in factors_2:
        factors_b = [right_factor(num,factors_1)] + factors_b
    factors = set(factors_a + factors_b)
    return factors
    
def mcm(num_1, num_2):
    mcm = 1
    lista = mcm_factors(num_1, num_2)
    for factor in lista:
        mcm = factor[0]**factor[1] * mcm
    return mcm

if __name__ == "__main__":
    from time import sleep
    print("Calcolatore di Minimo Comune Multiplo")
    num_1 = int(input("Immettere il primo numero: "))
    num_2 = int(input("Immettere il secondo numero: "))
    print(mcm(num_1, num_2))
